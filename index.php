<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
    <title>To-Do List</title>
  </head>
  <body>
    
    <h1 class="text-center py-4 my-4">To-Do List</h1>

    <div class="w-50 m-auto">
    <form action="data.php" method="post" autocomplete="off">
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" type="text" name="title" id="title" placeholder="Enter Your Task..." Required>

        </div><br>
        <button class="btn btn-primary">Add Task</button>

    </form>

    </div><br>
    <hr class="bg-dark w-50 m-auto">
    <div class="lists w-50 m-auto my-4">
        <h1>Tasks</h1>
        <div id="lists">
        <table class="table table-hover">
  <thead>
    <tr>
    <th scope="col">Tasks</th>
    <th scope="col" name="created_at">Date</th>
    <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
        include 'database.php';
        $sql="SELECT * FROM todo";
        $result=mysqli_query($conn, $sql);

        if($result){
            while($row=mysqli_fetch_assoc($result)){
                $id=$row['id'];
                $title=$row['title'];
                $date=$row['created_at'];
               


                ?>
                <tr class="to bg-warning">
                    <td><?php echo $title ?></td>
                    <td>
                    <a class="togg btn btn-success btn-sm" href="#" data-taskid="<?php echo $id ?>"><svg xmlns="http://www.w3.org/2000/svg" height="1.12em" viewBox="0 0 448 512"><style>svg{fill:#ffffff; margin-right: 5px;}</style><path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z"/></svg>Done</a>
                    <a class="btn btn-primary btn-sm" href="edit.php?id=<?php echo $id ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" height="0.95em" viewBox="0 0 512 512"><style>svg{fill:#ffffff; margin-right: 5px;}</style><path d="M362.7 19.3L314.3 67.7 444.3 197.7l48.4-48.4c25-25 25-65.5 0-90.5L453.3 19.3c-25-25-65.5-25-90.5 0zm-71 71L58.6 323.5c-10.4 10.4-18 23.3-22.2 37.4L1 481.2C-1.5 489.7 .8 498.8 7 505s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2L421.7 220.3 291.7 90.3z"/></svg>Edit</a>
                    <a class="btn btn-danger btn-sm" href="delete.php?id=<?php echo $id ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512"><style>svg{fill:#ffffff; margin-bottom: 4px; margin-left: 2px;}</style><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"/></svg></a>
                    </td>
                    <input type="hidden" class="taskId" value="<?php echo $id ?>">
                    <td><?php echo $date ?></td>
                </tr>

                <?php   
            }
        };
    ?>
    
   
  </tbody>
</table>
        </div>
    </div>

    <!-- Option 3: JavaScript jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
$(document).ready(function() {
    $(".togg").each(function() {
        var taskId = $(this).data("taskid");
        var doneStatus = localStorage.getItem('task_' + taskId);
        
        if (doneStatus === '1') {
            $(this).text("Undo").css("background-color", "green");
            $(this).closest("tr").removeClass("bg-warning").addClass("bg-secondary");
        }
    });
    
    $(".togg").click(function(e) {
        e.preventDefault(); 
        
        var clickedButton = $(this);
        var taskId = clickedButton.data("taskid");
        var doneStatus = localStorage.getItem('task_' + taskId);
        
      
        if (doneStatus === '1') {
            localStorage.setItem('task_' + taskId, '0');
        } else {
            localStorage.setItem('task_' + taskId, '1');
        }

        $.post("switch.php", { task_id: taskId }, function(data) {
  
            if (doneStatus === '1') {
               clickedButton.html('<svg xmlns="http://www.w3.org/2000/svg" height="1.12em" viewBox="0 0 448 512"><style>svg{fill:#ffffff; margin-right: 5px;}</style><path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z"/></svg> Done').css("background-color", "");

               clickedButton.closest("tr").removeClass("bg-secondary").addClass("bg-warning");
            }else {
               clickedButton.html('<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><style>svg{fill:#ffffff}</style><path d="M48.5 224H40c-13.3 0-24-10.7-24-24V72c0-9.7 5.8-18.5 14.8-22.2s19.3-1.7 26.2 5.2L98.6 96.6c87.6-86.5 228.7-86.2 315.8 1c87.5 87.5 87.5 229.3 0 316.8s-229.3 87.5-316.8 0c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0c62.5 62.5 163.8 62.5 226.3 0s62.5-163.8 0-226.3c-62.2-62.2-162.7-62.5-225.3-1L185 183c6.9 6.9 8.9 17.2 5.2 26.2s-12.5 14.8-22.2 14.8H48.5z"/></svg> Undo').css("background-color", "green");
               clickedButton.closest("tr").removeClass("bg-warning").addClass("bg-secondary");
            }
        });
    });
});
</script>
    


    <!-- Option 2: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 3: Separate Popper and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>

  </body>
</html>